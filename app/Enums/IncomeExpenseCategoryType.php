<?php

namespace App\Enums;

enum IncomeExpenseCategoryType: string
{
    case INCOME = 'INCOME';
    case EXPENSE = 'EXPENSE';

    public static function getType(string $type): IncomeExpenseCategoryType
    {
        return match($type)
        {
            'INCOME' => IncomeExpenseCategoryType::INCOME,
            'EXPENSE' => IncomeExpenseCategoryType::EXPENSE,
        };
    }
}
