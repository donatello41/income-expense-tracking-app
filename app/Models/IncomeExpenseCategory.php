<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class IncomeExpenseCategory extends Model
{
    protected $table = 'income_expense_categories';
    protected $fillable = ['name', 'type', 'user_id', 'created_at', 'updated_at'];
    protected $dates = ['created_at', 'updated_at'];

    public function incomeExpense(): HasMany
    {
        return $this->hasMany(IncomeExpense::class);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
