<?php

namespace App\Actions;

use App\Models\IncomeExpense;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Lorisleiva\Actions\Concerns\AsAction;
use Illuminate\Validation\Rule;

class UpdateIncomeExpense
{
    use AsAction;

    public function handle(?float $amount, ?string $currency, ?string $description, ?int $category_id, string $transaction_date, IncomeExpense $record): IncomeExpense
    {
        $user = Auth::user();
        $record->transaction_date = $transaction_date ?? $record->transaction_date ;
        $record->amount = $amount ?? $record->amount;
        $record->currency = $currency ?? $record->currency;
        $record->description = $description ?? $record->description;
        $record->user_id = $user->id ?? $record->user_id;
        $record->income_expense_category_id = $category_id ?? $record->income_expense_category_id;
        $record->save();

        return $record;
    }

    public function asController(Request $request, IncomeExpense $record): JsonResponse|IncomeExpense
    {
        $acceptHeader = $request->header('Accept');
        if ($acceptHeader !== 'application/json') {
            return response()->json(['message' => 'Yanlış http header beklenen Accept: application/json'], 406);
        }

        return $this->handle(
            $request->amount,
            $request->currency,
            $request->description,
            $request->category_id,
            $request->transaction_date,
            $record
        );
    }

    public function jsonResponse(IncomeExpense $incomeExpense): JsonResponse
    {
        return response()->json([
            'success' => true,
            'data' => $incomeExpense->toArray(),
            'message' => 'Kayıt güncellendi ',
        ]);
    }

    public function getValidationMessages(): array
    {
        return [
            'category_id.exists' => 'Kategori bulunamadı',
            'currency.in' => 'Para birimi yanlış',
        ];
    }

    public function rules(): array
    {
        return [
            'category_id' => 'exists:income_expense_categories,id',
            'currency' => [Rule::in(['TRY', 'USD', 'EUR'])],
        ];
    }
}
