<?php

namespace App\Enums;

enum Currency: string
{
    case TRY = 'TRY';
    case USD = 'USD';
    case EUR = 'EUR';

    public static function getType(string $type): Currency
    {
        return match($type)
        {
            'TRY' => Currency::TRY,
            'USD' => Currency::USD,
            'EUR' => Currency::EUR,
        };
    }
}
