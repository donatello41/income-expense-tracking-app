<?php

namespace App\Actions;

use App\Models\IncomeExpenseCategory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Lorisleiva\Actions\Concerns\AsAction;

class DeleteIncomeExpenseCategory
{
    use AsAction;

    public function handle(IncomeExpenseCategory $record): array
    {
        $record->delete();
        return [];
    }

    public function asController(Request $request, IncomeExpenseCategory $record): JsonResponse|array
    {
        $acceptHeader = $request->header('Accept');
        if ($acceptHeader !== 'application/json') {
            return response()->json(['message' => 'Yanlış http header beklenen Accept: application/json'], 406);
        }
        return $this->handle($record);
    }

    public function jsonResponse(): JsonResponse
    {
        return response()->json([
            'success' => true,
            'data' => [],
            'message' => 'Kayıt silindi',
        ]);
    }
}
