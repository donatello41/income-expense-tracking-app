<?php

namespace App\Actions;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Lorisleiva\Actions\Concerns\AsAction;
use App\Models\IncomeExpenseCategory;
use Auth;


class GetIncomeExpenseCategoriesByUser
{
    use AsAction;

    public function handle(): Collection
    {
        $user = Auth::user();
        return IncomeExpenseCategory::where('user_id', $user->id)->get();
    }

    public function asController(Request $request): Collection|JsonResponse
    {
        $acceptHeader = $request->header('Accept');
        if ($acceptHeader !== 'application/json') {
            return response()->json(['message' => 'Yanlış http header beklenen Accept: application/json', 'data' => []], 406);
        }

        return $this->handle();
    }

    public function jsonResponse(Collection $categories): JsonResponse
    {
        return response()->json([
            'message' => 'ok',
            'data' => $categories->toArray(),
        ]);
    }


}
