<?php

namespace App\Actions;

use App\Models\IncomeExpenseCategory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Lorisleiva\Actions\Concerns\AsAction;

class GetIncomeExpenseCategory
{
    use AsAction;

    public function handle(int $category_id): IncomeExpenseCategory
    {
        return IncomeExpenseCategory::find($category_id);
    }

    public function asController(Request $request): IncomeExpenseCategory|JsonResponse
    {
        $acceptHeader = $request->header('Accept');
        if ($acceptHeader !== 'application/json') {
            return response()->json(['message' => 'Yanlış http header beklenen Accept: application/json', 'data' => []], 406);
        }

        return $this->handle($request->category_id);
    }

    public function jsonResponse(IncomeExpenseCategory $category): JsonResponse
    {
        return response()->json([
            'message' => 'ok',
            'data' => $category->toArray(),
        ]);
    }

    public function getValidationMessages(): array
    {
        return [
            'category_id.required' => 'category_id alanı zorunludur',
            'category_id.integer' => 'category_id alanı integer bir değer olmalıdır',
        ];
    }

    public function rules(): array
    {
        return [
            'category_id' => 'required|integer',
        ];
    }
}
