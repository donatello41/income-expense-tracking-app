<?php

namespace App\Actions;

use App\Models\IncomeExpense;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Lorisleiva\Actions\Concerns\AsAction;
use Illuminate\Validation\Rule;

class CreateIncomeExpense
{
    use AsAction;

    public function handle(float $amount, string $currency, ?string $description, int $category_id, string $transaction_date): IncomeExpense
    {
        $user = Auth::user();
        return IncomeExpense::create([
            'transaction_date' => $transaction_date,
            'amount' => $amount,
            'currency' => $currency,
            'description' => $description,
            'user_id' => $user->id,
            'income_expense_category_id' => $category_id
        ]);
    }

    public function asController(Request $request)
    {
        $acceptHeader = $request->header('Accept');
        if ($acceptHeader !== 'application/json') {
            return response()->json(['message' => 'Yanlış http header beklenen Accept: application/json'], 406);
        }

        return $this->handle(
            $request->amount,
            $request->currency,
            $request->description,
            $request->category_id,
            $request->transaction_date
        );
    }

    public function jsonResponse(IncomeExpense $incomeExpense): JsonResponse
    {
        return response()->json([
            'success' => 'true',
            'category' => $incomeExpense->toArray(),
            'data' => 'Kayıt oluşturuldu ',
        ]);
    }

    public function getValidationMessages(): array
    {
        return [
            'amount.required' => 'Miktar gereklidir',
            'currency.required' => 'Para birimi gereklidir',
            'category_id.required' => 'Kategori gereklidir',
            'transaction_date.required' => 'İşlem tarihi gereklidir',
            'currency.in' => 'Para birimi yanlış',
        ];
    }

    public function rules(): array
    {
        return [
            'amount' => 'required',
            'category_id' => 'required',
            'transaction_date' => 'required',
            'currency' => ['required', Rule::in(['TRY', 'USD', 'EUR'])],
        ];
    }
}
