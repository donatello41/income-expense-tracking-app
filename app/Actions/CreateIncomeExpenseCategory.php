<?php

namespace App\Actions;

use App\Enums\IncomeExpenseCategoryType;
use App\Models\IncomeExpenseCategory;
use Auth;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Lorisleiva\Actions\Concerns\AsAction;
use Illuminate\Validation\Rule;

class CreateIncomeExpenseCategory
{
    use AsAction;

    public function handle(string $name, string $type): IncomeExpenseCategory
    {
        $user = Auth::user();
        return IncomeExpenseCategory::create([
            'name' => $name,
            'user_id' => $user->id,
            'type' => IncomeExpenseCategoryType::getType($type),
        ]);
    }

    public function asController(Request $request): JsonResponse|IncomeExpenseCategory
    {
        $acceptHeader = $request->header('Accept');
        if ($acceptHeader !== 'application/json') {
            return response()->json(['message' => 'Yanlış http header beklenen Accept: application/json'], 406);
        }

        return $this->handle(
            $request->name,
            $request->type,
        );
    }

    public function jsonResponse(IncomeExpenseCategory $category): JsonResponse
    {
        return response()->json([
            'message' => 'Kayıt oluşturuldu',
            'data' => $category->toArray(),
        ]);
    }

    public function getValidationMessages(): array
    {
        return [
            'name.required' => 'Ad alanı gereklidir',
            'type.required' => 'Gelir gider türü gereklidir',
            'type.in' => 'Gelir gider türü yanlış',
        ];
    }

    public function rules(): array
    {
        return [
            'name' => 'required',
            'type' => ['required', Rule::in(['INCOME', 'EXPENSE'])],
        ];
    }
}
