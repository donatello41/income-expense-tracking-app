<?php

namespace App\Actions;

use App\Models\IncomeExpense;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Lorisleiva\Actions\Concerns\AsAction;

class DeleteIncomeExpense
{
    use AsAction;

    public function handle(IncomeExpense $record): array
    {
        $record->delete();
        return [];
    }

    public function asController(Request $request, IncomeExpense $record): JsonResponse|array
    {
        $acceptHeader = $request->header('Accept');
        if ($acceptHeader !== 'application/json') {
            return response()->json(['message' => 'Yanlış http header beklenen Accept: application/json'], 406);
        }
        return $this->handle($record);
    }

    public function jsonResponse(): JsonResponse
    {
        return response()->json([
            'success' => true,
            'data' => [],
            'message' => 'Kayıt silindi',
        ]);
    }
}
