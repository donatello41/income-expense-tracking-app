<?php

use App\Actions\CreateIncomeExpense;
use App\Actions\CreateIncomeExpenseCategory;
use App\Actions\DeleteIncomeExpense;
use App\Actions\DeleteIncomeExpenseCategory;
use App\Actions\GetIncomeExpense;
use App\Actions\GetIncomeExpenseByUser;
use App\Actions\GetIncomeExpenseCategoriesByUser;
use App\Actions\GetIncomeExpenseCategory;
use App\Actions\UpdateIncomeExpense;
use App\Actions\UpdateIncomeExpenseCategory;
use App\Http\Controllers\Auth\ApiAuthController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', [ApiAuthController::class, 'register']);
Route::post('login', [ApiAuthController::class, 'login']);

Route::middleware('auth:api')->group(function () {

    Route::post('category/create', CreateIncomeExpenseCategory::class);
    Route::post('category/{record}/update', UpdateIncomeExpenseCategory::class);
    Route::delete('category/{record}/delete', DeleteIncomeExpenseCategory::class);
    Route::get('categories', GetIncomeExpenseCategoriesByUser::class);
    Route::post('category', GetIncomeExpenseCategory::class);


    Route::post('income-expense/create', CreateIncomeExpense::class);
    Route::post('income-expense/{record}/update', UpdateIncomeExpense::class);
    Route::delete('income-expense/{record}/delete', DeleteIncomeExpense::class);
    Route::get('income-expense', GetIncomeExpenseByUser::class);
    Route::post('income-expense-record', GetIncomeExpense::class);
});
