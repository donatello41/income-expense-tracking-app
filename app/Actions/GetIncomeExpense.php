<?php

namespace App\Actions;

use App\Models\IncomeExpense;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Lorisleiva\Actions\Concerns\AsAction;

class GetIncomeExpense
{
    use AsAction;

    public function handle(int $record_id): IncomeExpense
    {
        return IncomeExpense::find($record_id);
    }

    public function asController(Request $request): IncomeExpense|JsonResponse
    {
        $acceptHeader = $request->header('Accept');
        if ($acceptHeader !== 'application/json') {
            return response()->json(['message' => 'Yanlış http header beklenen Accept: application/json', 'data' => []], 406);
        }

        return $this->handle($request->id);
    }

    public function jsonResponse(IncomeExpense $record): JsonResponse
    {
        return response()->json([
            'message' => 'ok',
            'data' => $record->toArray(),
        ]);
    }

    public function getValidationMessages(): array
    {
        return [
            'id.required' => 'id alanı zorunludur',
            'id.integer' => 'id alanı integer bir değer olmalıdır',
        ];
    }

    public function rules(): array
    {
        return [
            'id' => 'required|integer',
        ];
    }
}
