<?php

namespace App\Actions;

use App\Models\IncomeExpenseCategory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Lorisleiva\Actions\Concerns\AsAction;
use Illuminate\Validation\Rule;

class UpdateIncomeExpenseCategory
{
    use AsAction;

    public function handle(?string $name, ?string $type, IncomeExpenseCategory $record): IncomeExpenseCategory
    {

        $record->type = $type ?? $record->type;
        $record->name = $name ?? $record->name;

        $record->save();

        return $record;
    }

    public function asController(Request $request, IncomeExpenseCategory $record): JsonResponse|IncomeExpenseCategory
    {
        $acceptHeader = $request->header('Accept');
        if ($acceptHeader !== 'application/json') {
            return response()->json(['message' => 'Yanlış http header beklenen Accept: application/json'], 406);
        }

        return $this->handle(
            $request->name,
            $request->type,
            $record
        );
    }

    public function jsonResponse(IncomeExpenseCategory $incomeExpense): JsonResponse
    {
        return response()->json([
            'message' => 'Kayıt güncellendi ',
            'data' => $incomeExpense->toArray(),
        ]);
    }

    public function getValidationMessages(): array
    {
        return [
            'type.in' => 'Kategori bilgisi yanlış',
        ];
    }

    public function rules(): array
    {
        return [
            'type' => [Rule::in(['INCOME', 'EXPENSE'])],
        ];
    }
}
