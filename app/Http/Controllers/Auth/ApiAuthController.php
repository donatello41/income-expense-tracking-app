<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\User;


class ApiAuthController extends Controller
{
    public function register (Request $request) {

        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ], [
            'name.required' => "Ad alanı gereklidir",
            'email.required' => "E-posta alanı gereklidir",
            'email.unique' => "Bu E-posta daha önce kullanılmış",
            'password.required' => 'Sifre alanı gereklidir',
            'password.confirmed' => 'Sifreler Eşleşmiyor',
            'password.min' => 'Sifre en az 6 karakterden olusmalıdır',
            ]);

        if ($validator->fails())
        {
            return response(['message' => $validator->errors()->all()], 422);
        }
        $request['password'] = bcrypt($request['password']);
        $user = User::create($request->toArray());

        return response()->json(['message' => 'Kullanıcı oluşturuldu' ,'user' => $user], 200);
    }

    public function login(Request $request)
    {
        $data = [
            'email' => $request->email,
            'password' => $request->password
        ];

        if (auth()->attempt($data)) {
            $accessToken = auth()->user()->createToken('authToken')->accessToken;
            return response()->json(['message'=>'success', 'accessToken' => $accessToken, 'user' => auth()->user()], 200);
        } else {
            return response()->json(['error' => 'Unauthorised'], 401);
        }
    }
}
