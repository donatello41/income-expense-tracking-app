<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('income_expense', function (Blueprint $table) {

            $table->increments('id');
            $table->timestamp('transaction_date');
            $table->float('amount')->unsigned();
            $table->enum('currency', ['TRY', 'USD', 'EUR']);
            $table->text('description')->nullable();

            $table->integer('user_id')->unsigned();
            $table->integer('income_expense_category_id')->unsigned()->nullable();

            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('income_expense_category_id')->references('id')->on('income_expense_categories')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('income_expense');
    }
};
