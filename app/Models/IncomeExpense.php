<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class IncomeExpense extends Model
{
    protected $table = 'income_expense';
    protected $fillable = ['transaction_date', 'amount', 'currency', 'description', 'user_id', 'income_expense_category_id'];
    protected $dates = ['transaction_date'];

    public function category(): BelongsTo
    {
        return $this->belongsTo(IncomeExpenseCategory::class);
    }
}
